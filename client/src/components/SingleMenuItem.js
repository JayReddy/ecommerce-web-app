import { Link } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

const SingleMenuItem = ({ url, icon, label }) => {
  return (
    <Link
      as={RouterLink}
      to={url}
      fontSize="sm"
      letterSpacing="wide"
      textTransform="uppercase"
      mr="5"
      display="flex"
      alignItems="center"
      color="whiteAlpha.600"
      _hover={{ textDecor: "none", color: "white" }}
    >
      {icon}
      {label}
    </Link>
  );
};

export default SingleMenuItem;
