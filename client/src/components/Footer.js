import { Flex, Text } from "@chakra-ui/react";

const Footer = () => {
  return (
    <Flex as="footer" justifyContent="center" py="5" bgColor="gray.800">
      <Text color="cyan">
        Copyright {new Date().getFullYear()} Versatile. All Rights Reserved.
      </Text>
    </Flex>
  );
};

export default Footer;
